﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AspNet.Models;
using AspNet.Models.Entities;
using System.Security.Claims;
using AspNet.Helpers;

namespace AspNet.Controllers.api
{
    public class ContactsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ContactsApi
        public IList<Contact> GetContacts()
        {
            int userCompanyId = User.GetCompanyId();
            return db.Contacts.Where(c => c.CompanyId == userCompanyId).ToList();
        }

        // GET: api/ContactsApi/5
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> GetContact(int id)
        {
            Contact contact = await db.Contacts.FindAsync(id);
            if (contact == null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        // PUT: api/ContactsApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContact(int id, Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contact.Id)
            {
                return BadRequest();
            }

            Contact dbModel = await db.Contacts.FindAsync(id);
            dbModel.FirstName = contact.FirstName;
            dbModel.LastName = contact.LastName;
            dbModel.Phone = contact.Phone;
            dbModel.Mobile = contact.Mobile;
            dbModel.Email = contact.Email;
            dbModel.Address1 = contact.Address1;
            dbModel.Address2 = contact.Address2;
            dbModel.AddressArea = contact.AddressArea;
            dbModel.AddressState = contact.AddressState;
            dbModel.AddressTown = contact.AddressTown;
            dbModel.Pincode = contact.Pincode;
            dbModel.UpdateCount = dbModel.UpdateCount + 1;
            dbModel.UpdatedBy = User.Identity.Name;
            dbModel.UpdatedOn = DateTime.UtcNow;

            //db.Entry(contact).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ContactsApi
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> PostContact(Contact contact)
        {
            contact.CreatedBy = User.Identity.Name;
            contact.CreatedOn = DateTime.UtcNow;
            contact.CompanyId = User.GetCompanyId();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Contacts.Add(contact);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = contact.Id }, contact);
        }

        // DELETE: api/ContactsApi/5
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> DeleteContact(int id)
        {
            Contact contact = await db.Contacts.FindAsync(id);
            if (contact == null)
            {
                return NotFound();
            }

            db.Contacts.Remove(contact);
            await db.SaveChangesAsync();

            return Ok(contact);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactExists(int id)
        {
            return db.Contacts.Count(e => e.Id == id) > 0;
        }
    }
}