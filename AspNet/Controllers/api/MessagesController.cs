﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AspNet.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using AspNet.Helpers;

namespace AspNet.Controllers.api
{
    public class MessagesController : ApiController
    {
        private EntityDataModel db = new EntityDataModel();

        // GET: api/Messages
        public IList<MessageListModel> GetMessages()
        {
            int userCompanyId = User.GetCompanyId();
            List<MessageListModel> items;

            using (var context = new EntityDataModel())
            {
                // Get comaniies (should be 1) where user is Admin (Role = 1)
                items = context.Database.SqlQuery<MessageListModel>(
                    @"SELECT
                        *
                    FROM
                        Message
                    WHERE
                        CompanyId IS NULL OR CompanyId = @p0", userCompanyId).ToList();
            }

            return items;
        }

        // GET: api/Messages/5
        [ResponseType(typeof(Message))]
        public async Task<IHttpActionResult> GetMessage(int id)
        {
            Message message = await db.Messages.FindAsync(id);
            if (message == null)
            {
                return NotFound();
            }

            return Ok(message);
        }

        // PUT: api/Messages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMessage(int id, Message message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != message.Id)
            {
                return BadRequest();
            }

            Message dbModel = await db.Messages.FindAsync(id);
            dbModel.UpdateCount = dbModel.UpdateCount + 1;
            dbModel.UpdatedBy = User.Identity.Name;
            dbModel.UpdatedOn = DateTime.UtcNow;

            //db.Entry(message).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Messages
        [ResponseType(typeof(Message))]
        public async Task<IHttpActionResult> PostMessage(Message message)
        {
            message.CreatedBy = User.Identity.Name;
            message.CreatedOn = DateTime.UtcNow;
            message.CompanyId = User.GetCompanyId();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Messages.Add(message);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = message.Id }, message);
        }

        // DELETE: api/Messages/5
        [ResponseType(typeof(Message))]
        public async Task<IHttpActionResult> DeleteMessage(int id)
        {
            Message message = await db.Messages.FindAsync(id);
            if (message == null)
            {
                return NotFound();
            }

            db.Messages.Remove(message);
            await db.SaveChangesAsync();

            return Ok(message);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MessageExists(int id)
        {
            return db.Messages.Count(e => e.Id == id) > 0;
        }

        public class MessageListModel
        {

            public int Id { get; set; }

            public int SenderId { get; set; }

            public int SenderName { get; set; }

            public int? CompanyId { get; set; }

            public string Title { get; set; }

            public string Body { get; set; }

            public DateTime? ActiveFromDate { get; set; }

            public DateTime? ActiveTillDate { get; set; }
        }
    }
}