﻿using AspNet.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Owin;
using System.Web;
using AspNet.Helpers;

namespace AspNet.Controllers.api
{
    [Authorize]
    public class MemberController : ApiController
    {
        [Route("api/Member/GetRoles")]
        public IEnumerable<string> GetRoles()
        {
            var userManager = HttpContext.Current.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = userManager.FindByEmail(User.Identity.Name);
            var claim = ((ClaimsIdentity)User.Identity).FindFirst("Nickname");

            var roles = new List<string>();

            if (user.IsAgent)
                roles.Add("IsAgent");
            if (user.IsDeveloper)
                roles.Add("IsDeveloper");
            if (user.IsLandlord)
                roles.Add("IsLandlord");
            if (user.IsBuyer)
                roles.Add("IsBuyer");
            if (user.IsTenant)
                roles.Add("IsTenant");

            return roles;
        }

        [Route("api/Member/GetUserCompanies")]
        public IEnumerable<CompanyViewModel> GetUserCompanies()
        {
            List<CompanyViewModel> companies;

            using (var context = new EntityDataModel())
            {
                // Get comaniies (should be 1) where user is Admin (Role = 1)
                companies = context.Database.SqlQuery<CompanyViewModel>(
                    @"SELECT
                        Company.*
                    FROM
                        UserCompany INNER JOIN
                        AspNetUsers ON UserCompany.UserId = AspNetUsers.Id INNER JOIN
                        Company ON UserCompany.CompanyId = Company.Id
                    WHERE UserCompany.RoleId = @p0 AND AspNetUsers.Id = @p1", UserRoles.Admin, User.Identity.GetUserId()).ToList();
            }

            if (companies.Any()) {
                var claimsIdentity = ((ClaimsIdentity)User.Identity);
                User.AddUpdateClaim("StaffOfCompanyId", companies[0].Id.ToString());
            }

            return companies;
        }

        [Route("api/Member/GetAllUserInfo")]
        public UserInfoViewModel GetAllUserInfo()
        {
            return new UserInfoViewModel
            {
                Roles = GetRoles(),
                HasCompany = GetUserCompanies().Any()
            };
        }

        // POST: api/Companies
        [HttpPost]
        [ResponseType(typeof(Company))]
        [Route("api/Member/SaveUserCompany", Name = "SaveUserCompany")]
        public async Task<IHttpActionResult> SaveUserCompany(Company company)
        {
            var db = new EntityDataModel();

            company.CreatedBy = User.Identity.Name;
            company.CreatedOn = DateTime.UtcNow;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Companies.Add(company);
            await db.SaveChangesAsync();

            var result = db.Database.ExecuteSqlCommand(
                    @"INSERT INTO UserCompany (UserId, CompanyId, RoleId) VALUES (@p0, @p1, @p2)",
                    User.Identity.GetUserId(),
                    company.Id,
                    1);


            User.AddUpdateClaim("StaffOfCompanyId", company.Id.ToString());


            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser model = userManager.FindById(User.Identity.GetUserId());
            model.StaffOfCompanyId = company.Id;
            IdentityResult updateResult = await userManager.UpdateAsync(model);

            //var response = Request.CreateResponse(HttpStatusCode.Created);
            //string uri = Url.Link("SaveUserCompany", new { id = company.Id });
            //response.Headers.Location = new Uri(uri);
            //return response;

            return CreatedAtRoute("SaveUserCompany", new { id = company.Id }, company);
        }

        public enum UserRoles {
            Admin=1
        }

        public class UserInfoViewModel
        {
            public IEnumerable<string> Roles { get; set; }
            public bool HasCompany { get; set; }
        }

        public class CompanyViewModel
        {

            public int Id { get; set; }

            public string Name { get; set; }

            public string Phone { get; set; }

            public string AltPhone { get; set; }

            public string Email { get; set; }

            public string Website { get; set; }

            public int CreatedByUserId { get; set; }

            public string Address1 { get; set; }

            public string Address2 { get; set; }

            public string AddressArea { get; set; }

            public string AddressTown { get; set; }

            public string AddressState { get; set; }

            public string Pincode { get; set; }

            public string UpdatedBy { get; set; }

            public DateTime? UpdatedOn { get; set; }

            public int UpdateCount { get; set; }

            public string CreatedBy { get; set; }

            public DateTime CreatedOn { get; set; }
        }
    }
}
