﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AspNet.Models.Entities;
using AspNet.Models;
using System.Security.Claims;
using AspNet.Helpers;

namespace AspNet.Controllers.api
{
    public class ActivitiesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Activities
        public IList<Activity> GetActivities()
        {
            int userCompanyId = User.GetCompanyId();
            return db.Activities.Where(c => c.CompanyId == userCompanyId).ToList();
        }

        // GET: api/Activities/5
        [ResponseType(typeof(Activity))]
        public async Task<IHttpActionResult> GetActivity(int id)
        {
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return NotFound();
            }

            return Ok(activity);
        }

        // PUT: api/Activities/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutActivity(int id, Activity activity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != activity.Id)
            {
                return BadRequest();
            }

            db.Entry(activity).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActivityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Activities
        [ResponseType(typeof(Activity))]
        public async Task<IHttpActionResult> PostActivity(PostActivityModel model)
        {
            model.Activity.CreatedBy = User.Identity.Name;
            model.Activity.CreatedOn = DateTime.UtcNow;
            model.Activity.CompanyId = User.GetCompanyId();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Activities.Add(model.Activity);
            var activityId = await db.SaveChangesAsync();

            foreach (int userId in model.Users)
            {
                db.ActivityScheduledForUser.Add(new ActivityScheduledForUser {
                    UserId = userId,
                    ActivityId = model.Activity.Id
                });
            }

            foreach (int contactId in model.Contacts)
            {
                db.ActivityScheduledWithContact.Add(new ActivityScheduledWithContact {
                    ContactId = contactId,
                    ActivityId = model.Activity.Id
                });
            }

            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = model.Activity.Id }, model);
        }

        // DELETE: api/Activities/5
        [ResponseType(typeof(Activity))]
        public async Task<IHttpActionResult> DeleteActivity(int id)
        {
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return NotFound();
            }

            db.Activities.Remove(activity);
            await db.SaveChangesAsync();

            return Ok(activity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActivityExists(int id)
        {
            return db.Activities.Count(e => e.Id == id) > 0;
        }

        public class PostActivityModel
        {
            public Activity Activity { get; set; }
            public int[] Users { get; set; }
            public int[] Contacts { get; set; }
        }
    }
}