﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace AspNet.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Name = ((ClaimsIdentity)User.Identity).Claims.Where(r => r.Type == "custom_name").FirstOrDefault().Value;

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Admin()
        {
            ViewBag.Message = "Your Admin page.";

            return View();
        }

        public ActionResult Agent()
        {
            ViewBag.Message = "Your Agent page.";
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = userManager.FindByEmail(User.Identity.Name);

            var roles = new List<string>();

            if (user.IsAgent)
                roles.Add("IsAgent");
            if (user.IsDeveloper)
                roles.Add("IsDeveloper");
            if (user.IsLandlord)
                roles.Add("IsLandlord");
            if (user.IsBuyer)
                roles.Add("IsBuyer");
            if (user.IsTenant)
                roles.Add("IsTenant");

            ViewBag.Roles = string.Join<string>(",", roles);
            ViewBag.CurrentRole = roles[0];
            return View();
        }

        public ActionResult Developer()
        {
            ViewBag.Message = "Your Developer page.";

            return View();
        }

        public ActionResult Landlord()
        {
            ViewBag.Message = "Your Landlord page.";

            return View();
        }

        public ActionResult Buyer()
        {
            ViewBag.Message = "Your Buyer page.";

            return View();
        }

        public ActionResult Tenant()
        {
            ViewBag.Message = "Your Tenant page.";

            return View();
        }
    }
}