﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AspNet.Models.Entities
{
    public class ActivityScheduledWithContact
    {
        [Key]
        [Column(Order = 1)]
        public int ActivityId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int ContactId { get; set; }
    }
}