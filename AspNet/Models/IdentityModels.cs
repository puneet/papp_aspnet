﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AspNet.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string Nickname { get; set; }
        public bool IsAgent { get; set; }
        public bool IsDeveloper { get; set; }
        public bool IsLandlord { get; set; }
        public bool IsBuyer { get; set; }
        public bool IsTenant { get; set; }
        public bool IsAdmin { get; set; }
        public int? StaffOfCompanyId { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here
            var user = manager.FindByName(userIdentity.Name);
            userIdentity.AddClaim(new Claim("Name", user.UserName));
            userIdentity.AddClaim(new Claim("StaffOfCompanyId", user.StaffOfCompanyId.ToString()));

            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<Contact> Contacts { get; set; }

        public System.Data.Entity.DbSet<Activity> Activities { get; set; }

        public System.Data.Entity.DbSet<AspNet.Models.Entities.ActivityScheduledWithContact> ActivityScheduledWithContact { get; set; }

        public System.Data.Entity.DbSet<AspNet.Models.Entities.ActivityScheduledForUser> ActivityScheduledForUser { get; set; }
    }
}