namespace AspNet.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EntityDataModel : DbContext
    {
        public EntityDataModel()
            : base("name=EntityDataModelConnection")
        {
        }

        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Lookup> Lookups { get; set; }
        public virtual DbSet<LookupType> LookupTypes { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<MessageReceiver> MessageReceivers { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectImage> ProjectImages { get; set; }
        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<PropertyFeature> PropertyFeatures { get; set; }
        public virtual DbSet<PropertyFeatureOption> PropertyFeatureOptions { get; set; }
        public virtual DbSet<PropertyImage> PropertyImages { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserCompany> UserCompanies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Properties<string>().Configure(c => c.IsUnicode(false));

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Contacts)
                .WithMany(e => e.Activities)
                .Map(m => m.ToTable("ActivityContact").MapLeftKey("ActivityId").MapRightKey("ContactId"));

            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Contacts1)
                .WithMany(e => e.Activities1)
                .Map(m => m.ToTable("ActivityUser").MapLeftKey("ActivityId").MapRightKey("UserId"));

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Activities)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Contacts)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Projects)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.UserCompanies)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Company1)
                .WithMany(e => e.Companies)
                .Map(m => m.ToTable("CompanyPartner").MapLeftKey("CompanyId").MapRightKey("PartnerCompanyId"));

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Properties)
                .WithMany(e => e.Companies)
                .Map(m => m.ToTable("CompanyProperty").MapLeftKey("CompanyId").MapRightKey("PropertyId"));

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Messages)
                .WithRequired(e => e.Contact)
                .HasForeignKey(e => e.SenderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.MessageReceivers)
                .WithRequired(e => e.Contact)
                .HasForeignKey(e => e.ReceiverId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contact>()
                .HasMany(e => e.Lookups)
                .WithMany(e => e.Contacts)
                .Map(m => m.ToTable("ContactType").MapLeftKey("ContactId").MapRightKey("ContactTypeId"));

            modelBuilder.Entity<Lookup>()
                .HasMany(e => e.Properties)
                .WithRequired(e => e.Lookup)
                .HasForeignKey(e => e.PropertyTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LookupType>()
                .HasMany(e => e.Lookups)
                .WithRequired(e => e.LookupType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Message>()
                .HasMany(e => e.Message1)
                .WithOptional(e => e.Message2)
                .HasForeignKey(e => e.ParentMessageId);

            modelBuilder.Entity<Message>()
                .HasMany(e => e.MessageReceivers)
                .WithRequired(e => e.Message)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.ProjectImages)
                .WithRequired(e => e.Project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Property>()
                .HasMany(e => e.PropertyImages)
                .WithRequired(e => e.Property)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PropertyFeature>()
                .HasMany(e => e.PropertyFeatureOptions)
                .WithRequired(e => e.PropertyFeature)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Properties)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserCompanies);
                //.WithRequired(e => e.User)
                //.WillCascadeOnDelete(false);
        }
    }
}
