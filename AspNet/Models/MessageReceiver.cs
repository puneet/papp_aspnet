namespace AspNet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MessageReceiver")]
    public partial class MessageReceiver
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MessageId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ReceiverId { get; set; }

        public bool HasRead { get; set; }

        public DateTime? ReadOn { get; set; }

        public virtual Contact Contact { get; set; }

        public virtual Message Message { get; set; }
    }
}
