namespace AspNet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PropertyFeatureOption")]
    public partial class PropertyFeatureOption
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int PropertyFeatureId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual PropertyFeature PropertyFeature { get; set; }
    }
}
