define(['knockout', 'services/message-service'], function(ko, messageService){

    return function ViewModel(){

        var self = this;

        self.list = ko.observableArray();

        self.activate = function(){
            messageService.get().then(self.list);
        };
    }
})