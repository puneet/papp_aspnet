define(['knockout', 'jquery', 'models/message-model', 'durandal/app', 'plugins/router', 'utils/form-helpers', 'services/message-service'], function(ko, $, MessageModel, app, router, formHelpers, messageService){

    return function(){
        var self = this;

        self.errors = ko.observableArray();
        self.editMode = ko.observable(false);
        self.model = {};

        self.activate = function(id){
            if (id == 'new') {
                self.editMode(false);
                self.model = new MessageModel();
            } else {
                self.editMode(true);
                messageService.getById(id).done(function (response) {
                    self.model = new MessageModel(response);
                });
            }
        };

        self.save = function() {

            var saveModel = Object.assign(self.model, {});
            messageService.addMessage(saveModel).done(function (result) {
                router.navigate('messages');
            }).fail(formHelpers.parseResponseAndDisplayErrors);
        };

        self.compositionComplete = function(){

            $('#startDate').datetimepicker();
            $('#endDate').datetimepicker({
                useCurrent: false //Important! See issue #1075
            });
            $("#startDate").on("dp.change", function (e) {
                $('#endDate').data("DateTimePicker").minDate(e.date);
                self.model.startDate(e.date);
            });
            $("#endDate").on("dp.change", function (e) {
                $('#startDate').data("DateTimePicker").maxDate(e.date);
                self.model.endDate(e.date);
            });
        };
    }
});