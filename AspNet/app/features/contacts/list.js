define(['knockout', 'services/contact-service'], function(ko, contactsService){

    return function ViewModel(){

        var self = this;

        self.list = ko.observableArray();

        self.activate = function(){
            contactsService.getContacts().then(function(response){
                self.list(response);
            });
        };
    }
})