define(['knockout', 'models/contact-model', 'durandal/app', 'plugins/router', 'utils/form-helpers', 'services/contact-service'], function(ko, ContactModel, app, router, formHelpers, contactService){

    return function(){
        var self = this;

        self.errors = ko.observableArray();
        self.editMode = ko.observable(false);
        self.model = {};

        self.activate = function(id){
            if (id == 'new') {
                self.editMode(false);
                self.model = new ContactModel();
            } else {
                self.editMode(true);
                return contactService.getById(id).done(function (response) {
                    self.model = new ContactModel(response);
                });
            }
        };

        self.save = function() {

            if (self.model.errors().length) {
                self.model.errors.showAllMessages();
            } else {

                var promise;
                if (self.editMode()) {
                    var saveModel = Object.assign(self.model, {});
                    promise = contactService.updateContact(saveModel).done(function (result) {
                        router.navigate('contacts');
                    });

                } else {
                    var saveModel = Object.assign(self.model, { });
                    promise = contactService.addContact(saveModel).done(function (result) {
                        router.navigate('contacts');
                    });
                }

                primise.fail(function (response) {
                    formHelpers.parseResponseAndDisplayErrors(self, response);
                });
            }
        };
    }
});