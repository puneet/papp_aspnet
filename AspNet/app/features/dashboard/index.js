﻿define(['durandal/app'], function (app) {
    var ctor = function () {
        this.displayName = 'Welcome to Ocam!';
        this.description = 'Ocam is a monitoring tool.';
        this.features = [
            'Clean MV* Architecture',
            'JS & HTML Modularity',
            'Make jQuery & Bootstrap widgets templatable and bindable (or build your own widgets).'
        ];
    };

    //Note: This module exports a function. That means that you, the developer, can create multiple instances.
    //This pattern is also recognized by Durandal so that it can create instances on demand.
    //If you wish to create a singleton, you should export an object instead of a function.
    //See the "flickr" module for an example of object export.

    return ctor;
});