﻿define(['knockout', 'models/company-model', 'durandal/app', 'plugins/router', 'utils/form-helpers', 'services/member-service'], function (ko, CompanyModel, app, router, formHelpers, memberService) {

    return function () {
        var self = this;

        self.errors = ko.observableArray();
        self.mode = ko.observable();

        self.activate = function (id) {
            if (id == 'new') {
                self.mode('new');
            }
            self.model = new CompanyModel();
        };

        self.save = function () {

            if (self.model.errors().length) {
                self.model.errors.showAllMessages();
            } else {

                var saveModel = Object.assign( self.model, { createdBy: "user" } );

                memberService.addUserCompanies(saveModel).done(function (result) {
                    app.userCompanyRequired(true);
                    router.navigate('contacts');
                }).fail(function (response) {
                    formHelpers.parseResponseAndDisplayErrors(self, response);
                });
            }
        };
    }
});