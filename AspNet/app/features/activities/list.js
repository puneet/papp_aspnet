define(['knockout', 'services/activity-service'], function(ko, activityService){

    return function ViewModel(){

        var self = this;

        self.list = ko.observableArray();

        self.activate = function(){
            activityService.get().then(function(response){
                self.list(response);
            });
        };
    }
})