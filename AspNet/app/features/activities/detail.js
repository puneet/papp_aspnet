define(['knockout', 'jquery', 'models/activity-model', 'durandal/app', 'plugins/router', 'utils/form-helpers', 'services/activity-service', 'services/contact-service', 'lookups/activity-types'], function (ko, $, ActivityModel, app, router, formHelpers, activityService, contactsService, activityTypes) {

    function User(arg) {
        var self = this;
        self.id = arg.id;
        self.name = arg.firstName + ' ' + arg.lastName;
        self.selected = ko.observable(false);
    }

    return function(){
        var self = this;

        self.errors = ko.observableArray();
        self.editMode = ko.observable(false);
        self.model = {};

        self.activityTypes = activityTypes.asArray;

        self.usersLookup = ko.observableArray();

        self.activate = function(id){

            contactsService.getUsers().then(function (response) {
                self.usersLookup(response.map(function (row) {
                    return new User(row)
                }));
            });

            if (id == 'new') {
                self.editMode(false);
                self.model = new ActivityModel();
            } else {
                self.editMode(true);
                return activityService.getById(id).done(function (response) {
                    self.model = new ActivityModel(response);
                });
            }
        };

        self.selectContactsToScheduledWith = function(){
            app.showDialog(
                'features/contacts/select-contacts',
                {},
                'ScrollingBackground'
            ).then(function(response){
                self.model.scheduledWith(response.contacts);
            });
        };

        self.removeContact = function (contact) {
            self.model.removeFromScheduledWith(contact);
        }

        self.save = function() {

            var activity = self.model;

            var activityToSave = {
                activity: {
                    title: activity.title(),
                    description: activity.description(),
                    startDate: activity.startDate(),
                    endDate: activity.endDate(),
                    activityTypeId: activity.type(),
                    scheduledById: 1 //@@@ hardcoding for now
                },
                contacts: activity.scheduledWith().map(function(item){
                    return item.id;
                }),
                users:  activity.scheduledFor(),
            }

            console.log(activityToSave);

            activityService.addActivity(activityToSave).done(function(result){
                console.log(result);
                router.navigate('activities');
            }).fail(function (response) {
                formHelpers.parseResponseAndDisplayErrors(self, response);
            });

            // if (self.contact.errors().length) {
            //     self.contact.errors.showAllMessages();
            // } else {

            //     var contactToSend = {
            //         contact: Object.assign(
            //             self.contact,
            //             {
            //                 first_name: self.contact.firstName,
            //                 last_name: self.contact.lastName,
            //                 types: null
            //             }
            //         )
            //     };

            //     activityService.addContact(contactToSend).done(function(result){
            //         console.log(result);
            //         router.navigate('contacts');
            //     }).fail(function(response){
            //         var responseText = JSON.parse(response.responseText);
            //         var errors = responseText.errors;
            //         var errorList = Object.keys(errors).map(function(key){
            //             self.contact[key].setError(errors[key][0]);
            //             return key + ' ' + errors[key][0];
            //         });
            //         // console.error(errorList);
            //         self.errors(errorList);
            //     });
            // }
        };

        self.compositionComplete = function(){

            //$('#users').dropdown();
            //$('#type').dropdown();

            //$('#rangeStart').calendar({
            //    type: 'datetime',
            //    endCalendar: $('#rangeEnd'),
            //    onChange: function(date, text){
            //        self.model.startDate(date);
            //    }
            //});

            //$('#rangeEnd').calendar({
            //    type: 'datetime',
            //    startCalendar: $('#rangeStart'),
            //    onChange: function(date, text){
            //        self.model.endDate(date);
            //    }
            //});

            //$('.input-daterange').datetimepicker({
            //});

            $('#startDate').datetimepicker();
            $('#endDate').datetimepicker({
                useCurrent: false //Important! See issue #1075
            });
            $("#startDate").on("dp.change", function (e) {
                $('#endDate').data("DateTimePicker").minDate(e.date);
                self.model.startDate(e.date);
            });
            $("#endDate").on("dp.change", function (e) {
                $('#startDate').data("DateTimePicker").maxDate(e.date);
                self.model.endDate(e.date);
            });
        };
    }
});