define(['knockout'], function(ko){

    return function SubscriptionModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;
        self.subscriber_id = arg.subscriber_id;

        self.from_date = ko.observable(arg.from_date).extend({ required: true, });
        self.to_date = ko.observable(arg.to_date).extend({ required: true });
        self.is_active = ko.observable(arg.is_active);
        self.internal_note = ko.observable(arg.internal_note);

        self.errors = ko.validation.group(self); //for ko validation
    };
});