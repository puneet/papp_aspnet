﻿define(['knockout'], function (ko) {

    return function ActivityModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;
        self.title = ko.observable(arg.title).extend({ required: true, maxLength: 250 });
        self.body = ko.observable(arg.body).extend({ required: true, maxLength: 5000 });
        self.activeFromDate = ko.observable(new Date(arg.activeFromDate)).extend({ required: true });
        self.activeToDate = ko.observable(new Date(arg.activeToDate)).extend({ required: true });

        self.errors = ko.validation.group(self); //for ko validation

    };
});