define(['knockout'], function(ko){

    return function ActivityModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;
        self.createdBy = ko.observable(arg.createdBy);
        self.createdWith = ko.observableArray(arg.createdWith ? JSON.parse(arg.createdWith) : undefined);
        self.createdOn = ko.observable(arg.createdOn);
        self.regarding = ko.observable(arg.regarding).extend({ required: true, maxLength: 250 });
        self.details = ko.observable(arg.details).extend({ required: true, maxLength: 500 });
        self.type = ko.observable(arg.type).extend({ required: true });
        self.attachments = ko.observableArray(arg.attachments);

        self.errors = ko.validation.group(self); //for ko validation
    };
});