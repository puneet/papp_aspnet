define(['knockout'], function(ko){

    return function SubscriberModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;
        self.name = ko.observable(arg.name).extend({ required: true, maxLength: 50 });

        self.address_line1 = ko.observable(arg.address_line1);
        self.address_line2 = ko.observable(arg.address_line2);
        self.address_area = ko.observable(arg.address_area);
        self.address_town = ko.observable(arg.address_town);
        self.address_state = ko.observable(arg.address_state);
        self.address_pin_code = ko.observable(arg.address_pin_code);
        self.post_code = ko.observable(arg.postcode);
        
        self.phone = ko.observable(arg.phone);
        self.alt_phone = ko.observable(arg.alt_phone).extend({ maxLength: 50 });
        
        self.email = ko.observable(arg.email);
        self.website = ko.observable(arg.website);
        //self.referredBy = ko.observable(arg.referredBy); // which marketing campaign got this customer

        self.property_types = ko.observableArray();
        self.areas_covered = ko.observable();

        self.errors = ko.validation.group(self); //for ko validation

    };
});