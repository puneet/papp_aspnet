define(['knockout'], function(ko){

    return function ActivityModel(arg) {
        arg = arg || {};
        var self = this;

        self.id = arg.id;

        self.url = ko.observable(arg.url);
        self.method = ko.observable(arg.method);
        self.data = ko.observable(arg.data);
        self.createdOn = ko.observable(arg.createdOn);
        self.syncedOn = ko.observable(arg.syncedOn);
        self.mergeErrors = ko.observable(arg.mergeErrors);
        self.syncSuccessful = ko.observable(arg.syncSuccessful);

    };
});