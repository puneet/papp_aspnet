﻿define([], function () {
    return {
        parseResponseAndDisplayErrors: function (viewModel, response) {
            var responseText = JSON.parse(response.responseText);
            var errors = responseText.modelState;
            var errorList = Object.keys(errors).map(function (key) {

                var camelizedKey = camelize(key.split(".").pop());
                viewModel.model[camelizedKey].setError(errors[key][0]);
                return errors[key][0];
            });
            viewModel.errors(errorList);

            function camelize(str) {
                return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
                    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
                    return index == 0 ? match.toLowerCase() : match.toUpperCase();
                });
            }
        }
    };
});