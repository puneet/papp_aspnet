﻿requirejs.config({
    paths: {
        'text': '../lib/require/text',
        'durandal':'../lib/durandal/js',
        'plugins' : '../lib/durandal/js/plugins',
        'transitions' : '../lib/durandal/js/transitions',
        'knockout': '../lib/knockout/knockout-3.4.0',
        'bootstrap': '../lib/bootstrap/js/bootstrap',
        'jquery': '../lib/jquery/jquery-1.9.1',
        'knockout-validation': '../lib/knockout-validation/knockout.validation',
        'moment': '../lib/moment/moment.min',
        'bootstrap-datetimepicker': '../lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min'
},
    shim: {
        'bootstrap': {
            deps: ['jquery'],
            exports: 'jQuery'
        },
        'knockout-validation': {
            deps: ['knockout']
        },
        'bootstrap-datetimepicker': {
            deps: ['bootstrap', 'moment']
        }
    }
});

define(['durandal/system', 'durandal/app', 'durandal/viewLocator', 'utils/durandal-extensions', 'knockout', 'knockout-validation', 'bootstrap', 'bootstrap-datetimepicker', 'binding-handlers/index'], function (system, app, viewLocator, durandalExtensions, ko, kov, bootstrap, datetimePicker, bindingHandlers) {
    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");

    app.title = 'Ocam';

    app.configurePlugins({
        router:true,
        dialog: true
    });

    durandalExtensions.addDialogContextWithScrollBar();

    app.start().then(function() {
        //Replace 'viewmodels' in the moduleId with 'views' to locate the view.
        //Look for partial views in a 'views' folder in the root.
        //viewLocator.useConvention();

        //Show the app by setting the root view model for our application with a transition.
        app.setRoot('shell');
    });

    ko.validation.init({
        insertMessages: true,
        messagesOnModified: true,
        decorateElementOnModified: true,
        decorateInputElement: true,
        errorElementClass: "error"
    });
});