﻿define(['jquery', 'knockout'], function ($, ko) {
    ko.bindingHandlers.datetimeRangePicker = {
        init: function (element, allValueAccessor, allBindingsAccessor) {

            //initialize datepicker with some optional options
            var bindingsAccessor = allBindingsAccessor();
            var valueAccessor = allValueAccessor();
            var options = bindingsAccessor.datepickerOptions || {};

            var rangeStartElement = $(valueAccessor.startElementSelector);
            var rangeEndElement = $(valueAccessor.endElementSelector);

            rangeStartElement.datetimepicker();
            rangeEndElement.datetimepicker({
                useCurrent: false //Important! See issue #1075
            });

            ko.utils.registerEventHandler(rangeStartElement, "dp.change", function (event) {
                rangeEndElement.data("DateTimePicker").minDate(event.date);
                valueAccessor.startObservable(event.date);
            });

            ko.utils.registerEventHandler(element, "changeDate", function (event) {
                rangeStartElement.data("DateTimePicker").maxDate(event.date);
                valueAccessor.endObservable(event.date);
            });
        },

        update: function (element, allValueAccessor) {

            var valueAccessor = allValueAccessor();
            var startWidget = $(valueAccessor.startElementSelector).data("DateTimePicker");
            var endWidget = $(valueAccessor.endElementSelector).data("DateTimePicker");

            if (startWidget) {
                startWidget.date(ko.utils.unwrapObservable(valueAccessor.startObservable));
            }

            if (endWidget) {
                endWidget.date(ko.utils.unwrapObservable(valueAccessor.endObservable));
            }
        }
    };
});