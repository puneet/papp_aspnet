define(['./lookup-helper'], function(lookupHelper) {

    var lookupTypeId = 2;
    //var types = lookupHelper.filterOutLookupType(lookupTypeId);
    var types = {
        1: 'Meeting',
        2: 'Phone call',
        3: 'Viewing',
    };

    return {
        asObject: types,
        asArray: Object.keys(types).map(function(key){
            return {
                id: key,
                caption: types[key]
            };
        })
    };

});