define(['./lookup-helper'], function(lookupHelper) {

    var lookupTypeId = 1;
    //var types = lookupHelper.filterOutLookupType(lookupTypeId);
    var types = {
        1: 'Landlord',
        2: 'Tenant',
        3: 'Buyer',
    };

    return {
        asObject: types,
        asArray: Object.keys(types).map(function(key){
            return {
                id: key,
                caption: types[key]
            };
        })
    };

});