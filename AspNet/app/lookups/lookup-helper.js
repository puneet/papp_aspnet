define(['durandal/app'], function(app) {

    return {
        filterOutLookupType: function(lookupTypeId) {


            var lookupData = [
                { lookupTypeId: 1, id: 1, name: 'Meeting' },
                { lookupTypeId: 1, id: 2, name: 'Meeting' },
                { lookupTypeId: 1, id: 3, name: 'Meeting' },
                { lookupTypeId: 1, id: 4, name: 'Meeting' },
                { lookupTypeId: 1, id: 5, name: 'Meeting' },
                { lookupTypeId: 1, id: 6, name: 'Meeting' },
            ];

            var lookupData = app.lookupData.filter(function(item) {
                return item.lookupTypeId == lookupTypeId;
            });

            var types = {};
            lookupData.forEach(function(row) {
                types[row.id] = row.name;
            });

            return types;
        }
    };

});