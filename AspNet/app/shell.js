﻿define(['knockout', 'plugins/router', 'durandal/app', 'services/member-service'], function (ko, router, app, memberService) {

    return function(){
        var self = this;

        self.router = router;

        self.userRoles = ko.observableArray();
        self.currentRole = ko.observable();

        self.search = function() {
            //It's really easy to show a message box.
            //You can add custom options too. Also, it returns a promise for the user's response.
            app.showMessage('Search not yet implemented...');
        };

        self.currentRole.subscribe(function (role) {
            if (role == "Agent" || role == "Developer") {
                if (app.userCompanyRequired()) {
                    router.navigate('companies/new');
                }
            } else {
                router.navigate('/');
            }
        });

        self.activate = function () {

            router.guardRoute = function (instance, instruction) {
                console.log("instance .....");
                console.log(instance);
                console.log("instruction ......");
                console.log(instruction);

                //if (app.user.companies && app.user.companies.length) {
                //    return 'companies/new'
                //}

                //if (app.user.isAuthenticated) {
                //    return true;
                //} else {
                //    if (instance && !instruction.config.allowAnonymous) {
                //        return 'login/' + instruction.fragment;
                //    }
                //    return true;
                //}
                return true;
            };

            router.map([
                { route: '', title: 'Dashboard', moduleId: 'features/dashboard/index', nav: true, allowedRoles: 'agent, developer, landlord' },
                { route: 'properties', title: 'Properties', moduleId: 'features/dashboard/index', nav: true, allowedRoles: 'agent, developer, landlord' },
                { route: 'contacts', title: 'Contacts', moduleId: 'features/contacts/list', nav: true, allowedRoles: 'agent, developer' },
                { route: 'contacts/:id', title: 'Contact', moduleId: 'features/contacts/detail', nav: false, allowedRoles: 'agent, developer' },
                { route: 'companies/:id', title: 'Company', moduleId: 'features/companies/detail', nav: false, allowedRoles: 'agent, developer' },
                { route: 'activities', title: 'Activities', moduleId: 'features/activities/list', nav: true, allowedRoles: 'agent, developer, landlord, buyer, tenant' },
                { route: 'activities/:id', title: 'Activities', moduleId: 'features/activities/detail', nav: false, allowedRoles: 'agent, developer, landlord, buyer, tenant' },
                { route: 'messages', title: 'Messages', moduleId: 'features/messages/list', nav: true, allowedRoles: 'agent, developer, landlord, buyer, tenant' },
                { route: 'messages/:id', title: 'Messages', moduleId: 'features/messages/detail', nav: false, allowedRoles: 'agent, developer, landlord, buyer, tenant' },
            ]).buildNavigationModel();

            router.activate();

            app.user = { companyRequired: ko.observable(false) };
            app.userCompanyRequired = ko.observable(false);

            /*
            memberService.getUserCompanies().done(function (response) {
                app.user.companies = response;
                if (!response.length) {
                    window.location.href = '#/companies/new';
                }
            });

            var promise = memberService.getRoles().done(function (response) {
                if (response.message) {
                    // Need to login
                    window.location.href = '/Account/Login';
                } else {
                    var roles = response.map(function (role) {
                        return role.replace("Is", "");
                    });

                    self.userRoles(roles);
                    app.user.roles = roles;

                    defineComputeds();
                }
            });
            */

            var promise = memberService.getAllUserInfo().done(function (response) {
                if (response.message) {
                    // Need to login
                    window.location.href = '/Account/Login';
                } else {
                    var roles = response.roles.map(function (role) {
                        if (!response.hasCompany && (role == "IsAgent" || role == "IsDeveloper")) {
                            app.user.companyRequired(true);
                            app.userCompanyRequired(true);
                        }
                        return role.replace("Is", "");
                    });

                    self.userRoles(roles);
                    app.user.roles = roles;

                    defineComputeds();
                }
            });

            function defineComputeds() {
                self.showMenu = ko.computed(function () {
                    var isPublicUser = self.currentRole() != "Agent" && self.currentRole() != "Developer";
                    var companyRequired = app.userCompanyRequired();
                    return isPublicUser || (!isPublicUser && !companyRequired);
                });

                self.routesForCurrentRole = ko.computed(function () {
                    var currentRole = self.currentRole();
                    var isPublicUser = self.currentRole() != "Agent" && self.currentRole() != "Developer";
                    var companyRequired = app.userCompanyRequired();
                    if (currentRole && self.showMenu()) {
                        currentRole = currentRole.toLowerCase();

                        return router.navigationModel().filter(function (route) {
                            return route.allowedRoles.indexOf(currentRole) > -1;
                        });
                    }
                });
            }

            return promise;
        };
    };
});