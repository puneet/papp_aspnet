define(['plugins/http', './config'], function(http, config){

    return {
        get: function(){
            return http.get(config.baseUrl + 'activities');
        },
        addActivity: function(contact){
            return http.post(config.baseUrl + 'activities', contact);
        },
        getById: function (id) {
            return http.get(config.baseUrl + 'activities/' + id);
        }
    }
});