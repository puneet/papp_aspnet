define(['plugins/http', './config'], function(http, config){

    return {
        getContacts: function(){
            return http.get(config.baseUrl + 'contacts');
        },
        getUsers: function(){
            // todo: right now it is getting all contacts, replace by users
            return http.get(config.baseUrl + 'contacts');
        },
        addContact: function (contact) {
            return http.post(config.baseUrl + 'contacts', contact);
        },
        updateContact: function (contact) {
            return http.put(config.baseUrl + 'contacts/' + contact.id, contact);
        },
        getById: function (id) {
            return http.get(config.baseUrl + 'contacts/' + id);
        }
    }
});