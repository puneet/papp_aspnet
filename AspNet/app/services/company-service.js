﻿define(['plugins/http', './config'], function (http, config) {

    return {
        addCompany: function (model) {
            return http.post(config.baseUrl + 'companies', model).then(function (response) {
                return response.data;
            });
        }
    }
});