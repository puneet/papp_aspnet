﻿define(['plugins/http', './config'], function (http, config) {
    return {
        get: function () {
            return http.get(config.baseUrl + 'messages');
        },
        addMessage: function (contact) {
            return http.post(config.baseUrl + 'messages', contact);
        },
        getById: function (id) {
            return http.get(config.baseUrl + 'messages/' + id);
        }
    }
});