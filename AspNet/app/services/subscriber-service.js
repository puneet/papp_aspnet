define(['plugins/http', './config'], function(http, config){

    var unwrappedResponse = function(response){
        return response.data;
    };

    return {
        get: function(){
            return http.get(config.baseUrl + 'subscribers').then(unwrappedResponse);
        },
        getById: function(id){
            return http.get(config.baseUrl + 'subscribers/' + id).then(unwrappedResponse);
        },
        getAllSubscriptions: function(subscriberId){
            return http.get(config.baseUrl + 'subscribers/' + subscriberId + '/subscriptions').then(unwrappedResponse);
        },
        getForUser: function(userId){
            return http.get(config.baseUrl + 'subscribers/get_all_for_user/' + userId).then(function(response){
                return response.data;
            });
        },
        addSubscriber: function(subscriber){
            return http.post(config.baseUrl + 'subscribers', subscriber).then(unwrappedResponse);
        },
        addSubscription: function(subscriberId, subscription){
            return http.post(config.baseUrl + 'subscribers/' + subscriberId + '/subscriptions', subscription).then(unwrappedResponse);
        }
    }
});