﻿define(['plugins/http', './config'], function (http, config) {
    return {
        getRoles: function () {
            return http.get(config.baseUrl + 'member/getroles');
        },
        getUserCompanies: function () {
            return http.get(config.baseUrl + 'member/GetUserCompanies');
        },
        addUserCompanies: function (model) {
            return http.post(config.baseUrl + 'member/SaveUserCompany', model);
        },
        getAllUserInfo: function () {
            return http.get(config.baseUrl + 'member/getalluserinfo');
        },
    }
});