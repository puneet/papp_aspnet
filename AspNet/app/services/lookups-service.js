define(['plugins/http', './config'], function(http, config){
    
    return {
        getLookupTypes: function(){
            return http.get(config.baseUrl + 'lookup_types').then(function(response){
                return response.data;
            });
        },
        getLookupValues: function(lookupTypeId){
            // TODO: Needs to pass type id and filter
            return http.get(config.baseUrl + 'lookup_values').then(function(response){
                return response.data;
            });
        },
        addLookupValue: function(data){
            return http.post(config.baseUrl + 'lookup_values', data);
        }
    }
});