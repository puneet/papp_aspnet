define(['plugins/http', './config'], function(http, config){

    return {
        addUser: function(model){
            return http.post(config.baseUrl + 'users', model).then(function(response){
                return response.data;
            });
        }
    }
});